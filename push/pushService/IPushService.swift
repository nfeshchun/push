//
// Created by Nikita Feshchun on 12.07.17.
// Copyright (c) 2017 Ronte. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
    
public protocol IPushSubscriber: class
{
    func send(notification: Dictionary<String,Any>)
}


public protocol IPushService: class
{
    var pushEnabled: Bool { get }
    
    func start()
    func stop()

    func setup(application: UIApplication)

    func subscribe(subscriber: IPushSubscriber)
    func unSubscribe(subscriber: IPushSubscriber)
    func localNotification(requestIdentifier: String, notification: UNMutableNotificationContent, delay: TimeInterval)
}
