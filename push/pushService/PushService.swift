//
// Created by Nikita Feshchun on 12.07.17.
// Copyright (c) 2017 Ronte. All rights reserved.
//

import Foundation
import FirebaseCommunity
import UserNotifications
import UIKit

open class PushService: NSObject, IPushService, UNUserNotificationCenterDelegate, MessagingDelegate
{
    public var pushEnabled: Bool = false

    private var subscribers: Dictionary<String,IPushSubscriber> = [:]

    public func setup(application: UIApplication)
    {
        if #available(iOS 10.0, *)
        {
            setupNewPush(application: application)
        }
        else
        {
            setupOldPush(application: application)
        }

        application.registerForRemoteNotifications()

        FirebaseApp.configure()
    }


    @available(iOS 10.0, *)
    fileprivate func setupNewPush(application: UIApplication)
    {
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })

        Messaging.messaging().delegate = self
    }

    
    fileprivate func setupOldPush(application: UIApplication)
    {
        let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        application.registerUserNotificationSettings(settings)
    }


    public func start()
    {
        pushEnabled = true
    }

    public func stop()
    {
        pushEnabled = false
    }

    public func subscribe(subscriber: IPushSubscriber)
    {
        subscribers[String(describing: subscriber.self)] = subscriber
    }

    public func unSubscribe(subscriber: IPushSubscriber)
    {
        subscribers[String(describing: subscriber.self)] = nil
    }

    public func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String)
    {
        if !pushEnabled
        {
            return
        }
    }

    public func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage)
    {
        guard pushEnabled,
              let aps = remoteMessage.appData as? Dictionary<String,Any>
                else
        {
            return
        }

        subscribers.values.forEach { subscriber in

            subscriber.send(notification: aps)

        }
    }

    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        guard pushEnabled,
              let userInfo = notification.request.content.userInfo as? Dictionary<String,Any>,
              let notification = userInfo["aps"] as? Dictionary<String,Any>
                else
        {
            return
        }

        subscribers.values.forEach { subscriber in

            subscriber.send(notification: notification)

        }
    }

    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        guard pushEnabled,
              let userInfo = response.notification.request.content.userInfo as? Dictionary<String,Any>,
              let notification = userInfo["aps"] as? Dictionary<String,Any>
                else
        {
            return
        }

        subscribers.values.forEach { subscriber in

            subscriber.send(notification: notification)

        }
    }

    public func localNotification(requestIdentifier: String, notification: UNMutableNotificationContent, delay: TimeInterval)
    {
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: delay, repeats: false)
        
        let request = UNNotificationRequest(identifier: requestIdentifier, content: notification, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error:Error?) in
            
            if error != nil {
                print(error?.localizedDescription ?? "Notification Register error")
            }
        }
    }
}
