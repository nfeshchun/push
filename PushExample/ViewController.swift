//
//  ViewController.swift
//  PushExample
//
//  Created by Nikita Feshchun on 13.07.17.
//  Copyright © 2017 Infotech. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, IPushSubscriber
{
    @IBOutlet weak var pushTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pushButton: UIButton!
    
    lazy var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    lazy var pushService: IPushService =
    { [unowned self] in
        return self.appDelegate.service
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pushService.subscribe(subscriber: self)
        pushService.start()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sendPushAction(_ sender: Any)
    {
        let notification = UNMutableNotificationContent()
        
        notification.badge = 12
        notification.title = "Test title"
        notification.subtitle = "Test subtitle"
        notification.body = "Test body"
        notification.categoryIdentifier = "actionCategory"
        notification.sound = UNNotificationSound.default()

        pushService.localNotification(requestIdentifier: "com.ronte.test-push", notification: notification, delay: 30.0)
    }

    @IBAction func enablePushAction(_ sender: Any)
    {
        if pushService.pushEnabled
        {
            pushService.stop()
            pushButton.setTitle("Push disabled", for: .normal)
        }
        else
        {
            pushService.start()
            pushButton.setTitle("Push enabled", for: .normal)
        }
        
    }
    
    //MARK: - IPushSubscriber
    
    func send(notification: Dictionary<String, Any>)
    {
        
    }
}

